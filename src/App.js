import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from '@firebase/app';
import reduxThunk from 'redux-thunk';

import Routes from './Routes';
import reducers from './reducers';

export default class App extends Component {

  componentWillMount() {

    const config = {
      apiKey: "YOUR-APP-KEY",
      authDomain: "YOUR-FIREBASE-DOMAIN",
      databaseURL: "YOUR-DATABASE-URL",
      projectId: "YOUR-PROJECT-ID",
      storageBucket: "YOUR-STORAGE-BUCKET",
      messagingSenderId: "YOUR-NUMBER-SEND-ID"
    };
    firebase.initializeApp(config);
  }

  render() {
    console.disableYellowBox = true; //Desabilitando os Warnings.
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(reduxThunk))}>
        <Routes />
      </Provider>
    )
  }
}