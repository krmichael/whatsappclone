import React from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';

import FormLogin from './components/FormLogin';
import FormCadastro from './components/FormCadastro';
import BoasVindas from './components/BoasVindas';
import Principal from './components/Principal';
import AdicionarContatos from './components/AdicionarContatos';
import Conversa from './components/Conversa';

const Routes = () => (
  <Router>
    <Stack navigationBarStyle={{backgroundColor: '#115E54'}} titleStyle={{color: '#fff'}} key='root'>
      <Scene
        key='login'
        component={FormLogin}
        title='Login'
        initial={true}
        hideNavBar={true}
      />
      <Scene
        key='cadastro'
        component={FormCadastro}
        title='Cadastre-se'
        hideNavBar={false}
      />
      <Scene
        key='boasVindas'
        component={BoasVindas}
        title='Bem Vindo(a)'
        hideNavBar={true}
      />
      <Scene
        key='principal'
        component={Principal}
        title='Principal'
        hideNavBar={true}
      />
      <Scene
        key='adicionar_contatos'
        component={AdicionarContatos}
        title='Adicionar Contatos'
        hideNavBar={false}
      />
      <Scene
        key='conversa'
        component={Conversa}
        title='Conversa'
        hideNavBar={false}
      />
    </Stack>
  </Router>
);

export default Routes;