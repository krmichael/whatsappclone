import React from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  TouchableHighlight
} from 'react-native';

import { connect } from 'react-redux';
import { TabBar } from 'react-native-tab-view';
import { Actions } from 'react-native-router-flux';
import { habilitaInclusaoContato } from '../actions/AppActions';
import firebase from '@firebase/app';
import '@firebase/auth';

const addContatos = require('../img/adicionar-contato.png');

const TabBarMenu = (props) => (
  <View style={styles.container}>

    <StatusBar backgroundColor={styles.statusBar} />

    <View style={styles.scopeBar}>
      <View style={styles.scopeTitle}>
        <Text style={styles.txtTitle}>WhatsApp Clone</Text>
      </View>

      <View style={styles.scopeItems}>
        <View style={styles.scopeIcon}>
          <TouchableHighlight
            onPress={() => {Actions.adicionar_contatos(); props.habilitaInclusaoContato()}}
            underlayColor={styles.underlayColor}>
            <Image source={addContatos} />
          </TouchableHighlight>
        </View>
        <View style={styles.scopeLogout}>
          <TouchableHighlight underlayColor='#115E54' onPress={() => firebase.auth().signOut().then(() => Actions.login())}>
            <Text style={styles.txtLogout}>Sair</Text>
          </TouchableHighlight>
        </View>
      </View>
    </View>

    <TabBar {...props} style={styles.tabBar} />
  </View>
);

export default connect(null, { habilitaInclusaoContato })(TabBarMenu);

const styles = {
  container: {
    backgroundColor: '#115E54',
    elevation: 4,
    marginBottom: 4
  },
  statusBar: '#114D44',
  scopeBar: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  scopeTitle: {
    height: 50,
    justifyContent: 'center'
  },
  txtTitle: {
    color: '#fff',
    fontSize: 20,
    marginLeft: 20
  },
  scopeItems: {
    flexDirection: 'row',
    marginRight: 20
  },
  scopeIcon: {
    width: 50,
    justifyContent: 'center'
  },
  underlayColor: '#115E54',
  scopeLogout: {
    justifyContent: 'center'
  },
  txtLogout: {
    color: '#fff',
    fontSize: 20
  },
  tabBar: {
    backgroundColor: '#115E54',
    elevation: 0
  }
}
