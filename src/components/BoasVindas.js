import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  Image,
  ImageBackground,
  StyleSheet
} from 'react-native';

import { Actions } from 'react-native-router-flux';

const bg = require('../img/bg.png')
const logo = require('../img/logo.png');

export default () => {
  return (
    <ImageBackground source={bg} style={{width: '100%', height: '100%'}}>
      <View style={styles.container}>

        <View style={styles.scopeTitle}>
          <Text style={styles.title}>Seja Bem Vindos</Text>
          <Image source={logo} />
        </View>

        <View style={styles.scopeButton}>
          <TouchableHighlight underlayColor='#115E54' style={styles.btnLogin} onPress={() => Actions.login()} accessibilityLabel='Fazer login'>
            <Text style={styles.txtLogin}>Fazer Login</Text>
          </TouchableHighlight>
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  scopeTitle: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 22,
    color: '#fff',
    marginBottom: 7
  },
  scopeButton: {
    flex: 1
  },
  btnLogin: {
    padding: 7,
    backgroundColor: '#115E54',
    borderRadius: 4,
    elevation: 4
  },
  txtLogin: {
    color: '#fff',
    fontSize: 18,
    alignSelf: 'center'
  }
});
