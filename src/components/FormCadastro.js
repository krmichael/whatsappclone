import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  ImageBackground,
  TouchableHighlight,
  StyleSheet,
  ActivityIndicator
} from 'react-native';

import { connect } from 'react-redux';

import {
  modificaNome,
  modificaEmail,
  modificaSenha,
  cadastraUsuario
} from '../actions/AutenticacaoActions';

const imgbg = require('../img/bg.png');

class FormCadastro extends Component {

  _cadastraUsuario = () => {
    const { nome, email, senha } = this.props;
    this.props.cadastraUsuario({ nome, email, senha });
  }

  renderBtnCadastro = () => {

    if(this.props.loading_cadastro) {
      return (
        <ActivityIndicator size='large'/>
      );
    }

    return (
      <TouchableHighlight
        style={styles.acessar}
        underlayColor='#115E54'
        onPress={() => this._cadastraUsuario()}
        accessibilityLabel='Cadastrar'>
        <Text style={styles.txtAcessar}>Cadastrar</Text>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      <ImageBackground source={imgbg} style={{ width: '100%', height: '100%' }}>
        <View style={styles.container}>
          <View style={{ flex: 4, justifyContent: 'center' }}>
            <TextInput
              value={this.props.nome}
              placeholder='Nome'
              placeholderTextColor='#fff'
              style={styles.input}
              onChangeText={(texto) => this.props.modificaNome(texto)}
            />
            <TextInput
              value={this.props.email}
              placeholder='E-mail'
              placeholderTextColor='#fff'
              style={styles.input}
              onChangeText={(texto) => this.props.modificaEmail(texto)}
            />
            <TextInput
              secureTextEntry={true}
              value={this.props.senha}
              placeholder='Senha'
              placeholderTextColor='#fff'
              style={styles.input}
              onChangeText={(texto) => this.props.modificaSenha(texto)}
            />

            <Text style={styles.msgErro}>{this.props.erroCadastro}</Text>
          </View>

          <View style={{ flex: 1 }}>
            {this.renderBtnCadastro()}
          </View>
        </View>
      </ImageBackground>
    )
  }
};

const mapStateToProps = (state) => ({
  nome: state.AutenticacaoReducer.nome,
  email: state.AutenticacaoReducer.email,
  senha: state.AutenticacaoReducer.senha,
  erroCadastro: state.AutenticacaoReducer.erroCadastro,
  loading_cadastro: state.AutenticacaoReducer.loading_cadastro
});

export default connect(mapStateToProps, {
  modificaNome,
  modificaEmail,
  modificaSenha,
  cadastraUsuario
})(FormCadastro);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  input: {
    fontSize: 15,
    height: 45,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  acessar: {
    backgroundColor: '#115E54',
    padding: 7,
    borderRadius: 4,
    elevation: 4
  },
  txtAcessar: {
    color: '#fff',
    fontSize: 18,
    alignSelf: 'center'
  },
  msgErro: {
    color: '#ff0000',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 5
  }
});