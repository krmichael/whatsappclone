import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  TextInput
} from 'react-native';

import { connect } from 'react-redux';
import { modificaAdicionaContatoEmail, adicionaContato } from '../actions/AppActions';

class AdicionarContatos extends React.Component {
  render() {

    if(this.props.cadastra_usuario_sucesso) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 18 }}>Cadastro realizado com sucesso!</Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>

        <View style={styles.scopeInput}>
          <TextInput
            placeholder='E-mail'
            style={styles.input}
            value={this.props.adiciona_contato_email}
            onChangeText={(texto) => this.props.modificaAdicionaContatoEmail(texto)}
          />
        </View>

        <View style={styles.scopeBtn}>
          <TouchableHighlight
            onPress={() => this.props.adicionaContato(this.props.adiciona_contato_email)}
            style={styles.btn}
            underlayColor={styles.underlayColor}>
            <Text style={styles.txtBtn}>Adicionar</Text>
          </TouchableHighlight>

          <Text style={{ color: '#ff0000', marginTop: 7 }}>
            {this.props.cadastro_resultado_txt_erro}
          </Text>
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  adiciona_contato_email: state.AppReducer.adiciona_contato_email,
  cadastro_resultado_txt_erro: state.AppReducer.cadastro_resultado_txt_erro,
  cadastra_usuario_sucesso: state.AppReducer.cadastra_usuario_sucesso
});


export default connect(mapStateToProps, {
  modificaAdicionaContatoEmail,
  adicionaContato
})(AdicionarContatos);

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20
  },
  scopeInput: {
    flex: 1,
    justifyContent: 'center'
  },
  input: {
    fontSize: 20,
    height: 45,
    borderBottomWidth: 1,
    borderBottomColor: '#115E54'
  },
  scopeBtn: {
    flex: 1,
    justifyContent: 'center'
  },
  btn: {
    padding: 7,
    backgroundColor: '#115E54',
    borderRadius: 4
  },
  underlayColor: '#115E54',
  txtBtn: {
    fontSize: 18,
    color: '#fff',
    alignSelf: 'center'
  }
}
