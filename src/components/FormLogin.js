import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableHighlight,
  ImageBackground,
  StyleSheet,
  ActivityIndicator
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { modificaEmail, modificaSenha, autenticaUsuario } from '../actions/AutenticacaoActions';

const imgbg = require('../img/bg.png');

class FormLogin extends Component {

  _autenticaUsuario = () => {
    const { email, senha } = this.props;

    this.props.autenticaUsuario({ email, senha });
  }

  renderBtnAcessar = () => {

    if(this.props.loading_login) {
      return (
        <ActivityIndicator size='large'/>
      )
    }
    return (
      <TouchableHighlight underlayColor='#115E54' style={styles.acessar} onPress={() => this._autenticaUsuario()} accessibilityLabel='Fazer login'>
        <Text style={styles.txtAcessar}>Acessar</Text>
      </TouchableHighlight>
    )
  }

  render() {
    return (
      <ImageBackground source={imgbg} style={{ width: '100%', height: '100%' }}>
        <View style={styles.container}>
          <View style={styles.title}>
            <Text style={styles.txtTitle}>WhatsApp Clone</Text>
          </View>

          <View style={{ flex: 2 }}>
            <TextInput
              value={this.props.email}
              placeholder='E-mail'
              placeholderTextColor='#fff'
              style={styles.input}
              onChangeText={(texto) => this.props.modificaEmail(texto)}
            />
            <TextInput
              secureTextEntry={true}
              value={this.props.senha}
              placeholder='Senha'
              placeholderTextColor='#fff'
              style={styles.input}
              onChangeText={(texto) => this.props.modificaSenha(texto)}
            />
            <TouchableHighlight underlayColor='transparent' onPress={() => Actions.cadastro()}>
              <Text style={styles.cadastre}>Ainda não tem cadastro? cadastre-se</Text>
            </TouchableHighlight>

            <Text style={styles.msgErro}>{this.props.erroLogin}</Text>
          </View>

          <View style={{ flex: 2 }}>
            {this.renderBtnAcessar()}
          </View>
        </View>
      </ImageBackground>
    )
  }
};

const mapStateToProps = (state) => ({
  email: state.AutenticacaoReducer.email,
  senha: state.AutenticacaoReducer.senha,
  erroLogin: state.AutenticacaoReducer.erroLogin,
  loading_login: state.AutenticacaoReducer.loading_login
});

export default connect(mapStateToProps, { modificaEmail, modificaSenha, autenticaUsuario })(FormLogin);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  title: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtTitle: {
    fontSize: 25,
    color: '#fff'
  },
  input: {
    fontSize: 15,
    height: 45,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  msgErro: {
    color: '#ff0000',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 5
  },
  cadastre: {
    fontSize: 15,
    marginTop: 10,
    color: '#fff'
  },
  acessar: {
    backgroundColor: '#115E54',
    padding: 7,
    borderRadius: 4,
    elevation: 4
  },
  txtAcessar: {
    color: '#fff',
    fontSize: 18,
    alignSelf: 'center'
  }
});